# Paddleguru

1. replay.html is the main file which contains the Javascript components for marker animation.

2. Server_files directory has 2 files out of which 
    * a.php - communicated with database
    * paddle.sql - can be imported to a database to restore the schema.

3. when the replay.html requests for data from the database it requests through a.php file which inturn responds with the following JSON

```[
   {
      "Athelete_Name":"LETOURNEUR MARTIN",
      "Bib_Number":"517",
      "Age_Group":"18-49",
      "Gender":"M",
      "Craft":"SUP 14",
      "Division":"1\/100",
      "Final_Placing":"1st",
      "Time":"2:10:30",
      "Time_sec":"7830"
   },
   {
      "Athelete_Name":"HASULYO BRUNO",
      "Bib_Number":"357",
      "Age_Group":"18-49",
      "Gender":"M",
      "Craft":"SUP 14",
      "Division":"2\/100",
      "Final_Placing":"2nd",
      "Time":"2:10:57",
      "Time_sec":"7857"
   }
]
```

### 4. known issues
    * Marker cluster is not displaying the number of markers in the cluster
    * Different colors for each marker was not achieved 
    * Bug in the infowindow of 0th marker (toggles between the start point and current location)
