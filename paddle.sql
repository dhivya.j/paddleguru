-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 30, 2017 at 07:41 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paddle`
--

-- --------------------------------------------------------

--
-- Table structure for table `athlete_info`
--

CREATE TABLE `athlete_info` (
  `Athelete_Name` varchar(1000) NOT NULL,
  `Bib_Number` varchar(100) NOT NULL,
  `Age_Group` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `Craft` varchar(100) NOT NULL,
  `Division` varchar(100) NOT NULL,
  `Final_Placing` varchar(100) NOT NULL,
  `Time` varchar(100) NOT NULL,
  `Time_sec` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `athlete_info`
--

INSERT INTO `athlete_info` (`Athelete_Name`, `Bib_Number`, `Age_Group`, `Gender`, `Craft`, `Division`, `Final_Placing`, `Time`, `Time_sec`) VALUES
('LETOURNEUR MARTIN', '517', '18-49', 'M', 'SUP 14', '1/100', '1st', '2:10:30', '7830'),
('HASULYO BRUNO', '357', '18-49', 'M', 'SUP 14', '2/100', '2nd', '2:10:57', '7857'),
('FREITAS MO', '326', '18-49', 'M', 'SUP 14\'', '3/100', '3rd', '2:12:07', '7927'),
('ARUTKIN ARTHUR', '531', '18-49', 'M', 'SUP 14\'', '4/100', '4th', '2:12:57\r\n', '7977'),
('TUNNINGTON TREVOR', '109', '18-49', 'M', 'SUP 14\'', '5/100', '5th', '2:13:33', '801');

-- --------------------------------------------------------

--
-- Table structure for table `course_info`
--

CREATE TABLE `course_info` (
  `Race_Name` varchar(10000) NOT NULL,
  `Race_Date` varchar(1000) NOT NULL,
  `Event_Name` varchar(10000) NOT NULL,
  `Start` varchar(10000) NOT NULL,
  `Intermediate` varchar(10000) NOT NULL,
  `Intermediate2` varchar(10000) NOT NULL,
  `End` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_info`
--

INSERT INTO `course_info` (`Race_Name`, `Race_Date`, `Event_Name`, `Start`, `Intermediate`, `Intermediate2`, `End`) VALUES
('West Marine Carolina Cup', '4/15/17', 'Long Course ', 'lat: 34.2044814, lng: -77.801652', 'lat: 34.207996, lng:-77.806072', 'lat: 34.210516, lng: -77.804913', 'lat: 34.214597, lng: -77.811157');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
