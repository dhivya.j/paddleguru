<?php
    //open connection to mysql db
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    $connection = mysqli_connect("localhost","root","","paddle") or die("Error " . mysqli_error($connection));

    $sql = "select * from athlete_info";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

     $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }

    echo json_encode($emparray);

     mysqli_close($connection);
?>